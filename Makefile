.PHONY: all clean test

O ?= .
ARCH ?=

ifeq ($(strip $(ARCH)),)
ARCH_ARM = y
ARCH_ARM64 = y
else ifeq (${ARCH}, arm)
ARCH_ARM = y
else ifeq (${ARCH}, arm64)
ARCH_ARM64 = y
endif

ifneq ("$(wildcard $(PWD)/MAINTAINERS)","")
LINUX ?= $(PWD)
else
mkfile_path := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
LINUX ?= ${mkfile_path}../linux
endif

DTC ?= ${LINUX}/scripts/dtc/dtc
DTC_WARN_FLAGS := -Wno-interrupt_provider -Wno-unit_address_vs_reg -Wno-unit_address_format -Wno-avoid_unnecessary_addr_size -Wno-alias_paths -Wno-graph_child_address -Wno-simple_bus_reg -Wno-unique_unit_address -Wno-pci_device_reg
DTC_FLAGS := -O dtb -@ -b 0 -i${LINUX}/include ${DTC_WARN_FLAGS}
CPP_FLAGS := -E -nostdinc -I${LINUX}/scripts/dtc/include-prefixes -undef -D__DTS__ -x assembler-with-cpp

dtbo_src-y =
dtbo_src-$(ARCH_ARM) += $(shell find arch/arm -name *.dtso)
dtbo_src-$(ARCH_ARM64) += $(shell find arch/arm64 -name *.dtso)
dtbo_src = ${dtbo_src-y}

dtbo_dst := $(dtbo_src:.dtso=.dtbo)
dtbo_tmp := $(dtbo_src:.dtso=.dtso.pre)

dtbo_dst := $(addprefix $(O)/,$(dtbo_dst))
dtbo_tmp := $(addprefix $(O)/,$(dtbo_tmp))

# List of all dts files that need symbols enabled.
# These are taken from $LINUX.

dtb_src_arm = \
	am335x-boneblack.dts \
	am437x-gp-evm.dts \
	am57xx-beagle-x15.dts \
	am57xx-beagle-x15-revb1.dts \
	am57xx-beagle-x15-revc.dts \
	am572x-idk.dts \
	am571x-idk.dts \
	am574x-idk.dts \
	dra7-evm.dts \
	dra72-evm.dts \
	dra72-evm-revc.dts \
	dra71-evm.dts \
	dra76-evm.dts \
	keystone-k2g-evm.dts

dtb_src_arm := $(dtb_src_arm:%=$(LINUX)/arch/arm/boot/dts/%)

dtb_src_arm64 = \
	ti/k3-am654-base-board.dts \
	ti/k3-j721e-common-proc-board.dts

dtb_src_arm64 := $(dtb_src_arm64:%=$(LINUX)/arch/arm64/boot/dts/%)

dtb_src-y =
dtb_src-$(ARCH_ARM) += $(dtb_src_arm)
dtb_src-$(ARCH_ARM64) += $(dtb_src_arm64)

dtb_src = $(dtb_src-y)
dtb_dst = $(dtb_src:$(LINUX)/%.dts=%.dtb)
dtb_tmp = $(dtb_src:$(LINUX)/%.dts=%.dts.pre)

dtb_dst := $(addprefix $(O)/,$(dtb_dst))
dtb_tmp := $(addprefix $(O)/,$(dtb_tmp))

all: dtbs dtbos

$(dtbo_tmp): $(O)/%.dtso.pre: %.dtso
	@mkdir -p `dirname $@`
	@gcc $(CPP_FLAGS) -o $@ $<

$(dtbo_dst): $(O)/%.dtbo: $(O)/%.dtso.pre
	@echo "  DTC $@"
	@$(DTC) $(DTC_FLAGS) -o $@ $<

dtbos: $(dtbo_dst)

$(dtb_tmp): $(O)/%.dts.pre : $(LINUX)/%.dts
	@mkdir -p `dirname $@`
	@gcc $(CPP_FLAGS) -o $@ $<

$(dtb_dst): $(O)/%.dtb : $(O)/%.dts.pre
	@echo "  DTC $@"
	@$(DTC) $(DTC_FLAGS) -i$(LINUX)/$(patsubst $(O)/%,%,$(dir $@)) -o $@ $<

dtbs: $(dtb_dst)

clean:
	@rm -f $(dtb_tmp) $(dtb_dst)
	@rm -f $(dtbo_tmp) $(dtbo_dst)

# base-dtb,ovl1,ovl2...
dtb-tests-arm := dra76-evm.dtb,ti/dra76-evm-tfp410.dtbo \
	dra71-evm.dtb,ti/dra71-evm-lcd-auo-g101evn01.0.dtbo \
	dra72-evm.dtb,ti/dra7x-evm-osd-lcd-common.dtbo,ti/dra72-evm-touchscreen.dtbo,ti/lcd-osd101t2045.dtbo \
	dra72-evm.dtb,ti/dra7x-evm-osd-lcd-common.dtbo,ti/dra72-evm-touchscreen.dtbo,ti/lcd-osd101t2587.dtbo \
	dra72-evm-revc.dtb,ti/dra7x-evm-osd-lcd-common.dtbo,ti/dra72-evm-touchscreen.dtbo,ti/lcd-osd101t2045.dtbo \
	dra72-evm-revc.dtb,ti/dra7x-evm-osd-lcd-common.dtbo,ti/dra72-evm-touchscreen.dtbo,ti/lcd-osd101t2587.dtbo \
	dra7-evm.dtb,ti/dra7x-evm-osd-lcd-common.dtbo,ti/dra74-evm-touchscreen.dtbo,ti/lcd-osd101t2045.dtbo \
	dra7-evm.dtb,ti/dra7x-evm-osd-lcd-common.dtbo,ti/dra74-evm-touchscreen.dtbo,ti/lcd-osd101t2587.dtbo

dtb-tests-arm += \
	am57xx-beagle-x15.dtb,ti/am57xx-evm-common.dtbo,ti/am57xx-evm.dtbo \
	am57xx-beagle-x15-revc.dtb,ti/am57xx-evm-common.dtbo,ti/am57xx-evm-reva3.dtbo

dtb-tests-arm += \
	am571x-idk.dtb,ti/am57xx-idk-osd-lcd-common.dtbo,ti/am571x-idk-touchscreen.dtbo,ti/lcd-osd101t2045.dtbo \
	am571x-idk.dtb,ti/am57xx-idk-osd-lcd-common.dtbo,ti/am571x-idk-touchscreen.dtbo,ti/lcd-osd101t2587.dtbo \
	am572x-idk.dtb,ti/am57xx-idk-osd-lcd-common.dtbo,ti/am572x-idk-touchscreen.dtbo,ti/lcd-osd101t2045.dtbo \
	am572x-idk.dtb,ti/am57xx-idk-osd-lcd-common.dtbo,ti/am572x-idk-touchscreen.dtbo,ti/lcd-osd101t2587.dtbo \
	am574x-idk.dtb,ti/am57xx-idk-osd-lcd-common.dtbo,ti/am572x-idk-touchscreen.dtbo,ti/lcd-osd101t2587.dtbo

dtb-tests-arm += \
	keystone-k2g-evm.dtb,ti/keystone-k2g-evm-lcd.dtbo

dtb-tests-arm64 := \
	ti/k3-am654-base-board.dtb,ti/k3-am654-sr1.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-base-board-sr1.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-gp.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-evm-hdmi.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-evm-oldi-lcd1evm.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-evm-tc358876.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-evm-ov5640.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-pcie-usb2.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-pcie-usb3.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-idk-sr1.dtbo \
	ti/k3-am654-base-board.dtb,ti/k3-am654-idk.dtbo

dtb-tests-arm64 += \
	ti/k3-j721e-common-proc-board.dtb,ti/k3-j721e-common-proc-board-infotainment.dtbo

comma := ,

define GEN_MERGE_CMD
@echo "  [TEST] $(subst $(comma), ,$1)"
@fdtoverlay -i $(2)$(word 1, $(subst $(comma), ,$1)) -o /tmp/test-merge-$(3).dtb $(addprefix $2,$(wordlist 2, 100, $(subst $(comma), ,$1)))

endef

test: test-arm test-arm64

test-arm: all
	$(foreach i,$(dtb-tests-arm),$(call GEN_MERGE_CMD,$(i),arch/arm/boot/dts/,arm))
	@rm -f /tmp/test-merge-arm.dtb

test-arm64: all
	$(foreach i,$(dtb-tests-arm64),$(call GEN_MERGE_CMD,$(i),arch/arm64/boot/dts/,arm64))
	@rm -f /tmp/test-merge-arm64.dtb
